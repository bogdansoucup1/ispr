
# import socket programming library
import socket

# import thread module
from _thread import *
from threading import Thread, Lock
import threading
import time
import model
import select

mutex = Lock()
rows = 6
cols = 7
messages = {"request_position": 0, "request_approved": 1, "invalid_request": 2, "position_not_available" : 3, "inform_player": 4, "login": 5, "invalid_credentials": 6, "add_to_queue": 7}
object_ids = {"player_one" : 1, "player_two" : 2}

def is_spot_free(game_map, column):
    index = 5
    mutex.acquire()
    while index >= 0 and game_map[index][column] != 0:
        index -= 1
    mutex.release()
    if index < 0:
        return False
    return True

def get_pos_from_column(game_map, column):
    index = 5
    mutex.acquire()
    while index >= 0 and game_map[index][column] != 0:
        index -= 1
    mutex.release()
    return (index, column)

def place_position(game_map, column, player):
    index = 5
    mutex.acquire()
    while index >= 0 and game_map[index][column] != 0:
        index -= 1
    game_map[index][column] = player
    mutex.release()

def get_message_at_index(message, index):
    mess = ""
    while index < len(message) and message[index] != '&':
        mess += message[index]
        index += 1
    return (mess, index)

def get_data_from_login_message(message):
    if int(message[0]) != messages["login"]:
        return None
    name = get_message_at_index(message, 2)
    password = get_message_at_index(message, name[1] + 1)
    return (name[0], password[0])


def get_login_message(message):
    converted_message = str(message.decode("utf-8"))
    Users = model.Model()
    Users.create_connection()
    (name, password) = get_data_from_login_message(converted_message)
    token = Users.login(name, password)
    if int(converted_message[0]) == messages["login"]:
        if len(token) > 30:
            print(Users.get_user_from_token(token))
            tuple_user = Users.get_user_from_token(token)
            return str(messages["request_approved"]) + "&" + tuple_user[1] + "&" + str(tuple_user[3])
        return str(messages["invalid_credentials"])
    else:
        return str(messages["invalid_request"])

def process_message(message, game_map, conn):
    converted_message = str(message.decode("utf-8"))
    if int(converted_message[0]) == messages["request_position"]:
        player = int(converted_message[1])
        column = int(converted_message[2])
        if is_spot_free(game_map, column):
            tuple_pos = get_pos_from_column(game_map, column)
            place_position(game_map, column, player)
            message_to_string = str(messages["request_approved"]) + str(player) + str(tuple_pos[0]) + str(tuple_pos[1])
            message_to_oposite_player = str(messages["inform_player"]) + str(player) + str(tuple_pos[0]) + str(tuple_pos[1])
            conn.send(message_to_oposite_player.encode('ascii'))
            return message_to_string
        else:
            return str(messages["position_not_available"])
    return str(messages["invalid_request"])

def inform_other_player(player, message):
    player_index = None
    if message[1] == object_ids["player_one"]:
        player_index = object_ids["player_two"]
    if message[2] == object_ids["player_two"]:
        player_index = object_ids["player_one"]
    return str(messages["inform_player"]) + str(player_index) + message[2] + message[3]

def is_player_turn(game_map, player):
    index_first_player = 0
    index_second_player = 0
    for rows in game_map:
        for elements in rows:
            if elements == object_ids["player_one"]:
                index_first_player += 1
            elif elements == object_ids["player_two"]:
                index_second_player += 1
    if (index_first_player == index_second_player and player == object_ids["player_one"]) or (index_first_player != index_second_player and player == object_ids["player_two"]):
        return False
    return True

def get_other_player(player):
    if player == object_ids["player_one"]:
        return object_ids["player_two"]
    if player == object_ids["player_two"]:
        return object_ids["player_one"]
    return None

def threaded(c, queue_match, game_maps):
    while True:
        message_to_send = "3"
        data = "Nothing"
        while str(message_to_send) == "3":
            if c in queue_match:
                data = queue_match[c].recv(1024)
            if not data:
                print('Player disconected')
                return
            message_to_send = process_message(data, game_maps[queue_match[c]], c)
            mutex.acquire()
            queue_match[c].send(message_to_send.encode('ascii'))
            mutex.release()
        message_to_send = "3"
        while str(message_to_send) == "3":
            if queue_match[c] in queue_match:
                data = c.recv(1024)
            if not data:
                print('Player disconected')
                return
            message_to_send = process_message(data, game_maps[c], queue_match[c])
            mutex.acquire()
            c.send(message_to_send.encode('ascii'))
            mutex.release()
            time.sleep( 0.1 )
    c.close()

def deep_copy(game):
    new_game = []
    for current_row in game:
        row_to_appen = []
        for current_col in current_row:
            row_to_appen.append(current_col)
        new_game.append(row_to_appen)
    return new_game

def get_score_and_name(mesg):
    index = 2
    name = ""
    score = ""
    while index < len(mesg) and mesg[index] != "&":
        name += mesg[index]
        index += 1
    index += 1
    while index < len(mesg) and mesg[index] != "&":
        score += mesg[index]
        index += 1
    return (name, int(score))

def login_thread(conn, queue):
    while True:
        data = conn.recv(1024)
        login_message = get_login_message(data)
        if data != None and int(login_message[0]) == messages["request_approved"]:
            conn.send(login_message.encode("ascii"))
            break
        else:
            conn.send(str(messages["invalid_credentials"]).encode("ascii"))
    add_to_queue_thread(conn, queue, get_score_and_name(login_message)[1])

def is_entering_queue_request(message):
    converted_message = str(message.decode("utf-8"))
    if int(converted_message[0]) == messages["add_to_queue"]:
        return True
    return False

def add_to_queue_thread(conn, queue, score):
    while True:
        message_recieved = None
        is_readable = [conn]
        is_writable = []
        is_error = []
        is_socket_full, w, e = select.select(is_readable, is_writable, is_error, 0.1)
        if is_socket_full:
            message_recieved = conn.recv(1024)
            if is_entering_queue_request(message_recieved):
                print("entered queue! ", message_recieved)
                mutex.acquire()
                queue.append((conn, time.time(), score))
                mutex.release()
                return True
        time.sleep( 0.1 )

def start_matche(first_conn, second_conn, queue_match, game_instances, queue, game):
    new_instance = deep_copy(game)
    first_conn.send("1".encode("ascii"))
    second_conn.send("2".encode("ascii"))
    game_instances[first_conn] = new_instance
    game_instances[second_conn] = new_instance
    start_new_thread(threaded, (queue[1][0], queue_match, game_instances,))

def async_queue(queue, game, queue_match, game_instances):
    while True:
        if len(queue) == 2:
            new_instance = deep_copy(game)
            queue_match[queue[0][0]] = queue[1][0]
            queue_match[queue[1][0]] = queue[0][0]
            data = queue[0][0].send("1".encode("ascii"))
            data = queue[1][0].send("2".encode("ascii"))
            game_instances[queue[0][0]] = new_instance
            game_instances[queue[1][0]] = new_instance
            start_new_thread(threaded, (queue[1][0], queue_match, game_instances,))
            queue.clear()
        time.sleep( 0.1 )
def Main():
    host = ""
    game_instances = {}

    game = [[0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0]]
    queue_map_connection = {}
    queue_match = {}

    queue = []
    sorting_hat = {}
    port = 12345
    active_queue = False
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((host, port))
    print("socket binded to port", port)
    s.listen(8)
    print("socket is listening")
    while True:
        conn, addr = s.accept()
        start_new_thread(login_thread, (conn, queue,))
        gpl = deep_copy(game)
        if active_queue == False:
            active_queue = True
            threading.Thread(target = async_queue, args = (queue, gpl, queue_match, game_instances,)).start()
        print('Connected to :', addr[0], ':', addr[1])
        time.sleep( 0.1 )
    s.close()


if __name__ == '__main__':
    Main()
