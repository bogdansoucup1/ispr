from tkinter import *
import tkinter.messagebox as tm


class LoginFrame(Frame):
    def __init__(self, master, socket, login_function, messages):
        super().__init__(master)

        self.label_username = Label(master, text="Username")
        self.label_password = Label(master, text="Password")

        self.label_password.place(relx=0.3, rely=0.3, anchor='n')
        self.label_username.place(relx=0.3, rely=0.35, anchor='n')

        self.entry_username = Entry(master)
        self.entry_password = Entry(master, show="*")

        self.entry_username.place(relx=0.5, rely=0.3, anchor='n')
        self.entry_password.place(relx=0.5, rely=0.35, anchor='n')
        self.logged = False

        self.checkbox = Checkbutton(master, text="Keep me logged in")
        self.checkbox.place(relx=0.5, rely=0.4, anchor='n')

        self.logbtn = Button(master, text="Login", command=self._login_btn_clicked)
        self.logbtn.place(relx=0.5, rely=0.45, anchor='n')
        self.socket = socket
        self.login_function = login_function
        self.messages = messages
        self.pack()

    def get_score_and_name(self, mesg):
        index = 2
        name = ""
        score = ""
        while index < len(mesg) and mesg[index] != "&":
            name += mesg[index]
            index += 1
        index += 1
        while index < len(mesg) and mesg[index] != "&":
            score += mesg[index]
            index += 1
        return (name, int(score))

    def _login_btn_clicked(self):
        username = self.entry_username.get()
        password = self.entry_password.get()
        self.socket.send(self.login_function(username, password))
        data = str(self.socket.recv(1024).decode("utf-8"))
        if data[0] == str(self.messages["request_approved"]):
            print(self.get_score_and_name(data))
            self.name, self.score = self.get_score_and_name(data)
            self.logged = True
        else:
            print(data)
            tm.showerror("Login error", "Incorrect username")