import tkinter as tk
from tkinter import Tk, Canvas, Frame, BOTH, NW
from PIL import Image, ImageTk
import random
import time
import socket
import select
import tkinter
import login_frame
import home_frame
import datetime
import final_frame

messages = {"request_position": 0, "request_approved": 1, "invalid_request": 2, "position_not_available" : 3, "inform_player": 4, "login": 5, "invalid_credentials": 6, "add_to_queue": 7}
players_indexes = {"player_one" : 1, "player_two" : 2, "empty_spages": 0}

def get_random_code():
    code = ""
    for index in range(6):
        code += chr(random.randint(97,122))
    return code

def pretty_print(game):
    for i in range(3):
        for j in range(3):
            print(game[i * 3 + j], end = " ")
        print()
    print()

def check(board):
    gridM = [[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0]]
    gridM = board
    #for i in range(6):
   #     for j in range(7):
   #         gridM[i][j] = board[i][j]
    for i in range(6):
        for j in range(4):
            ok = True
            for k in range(j, j + 3):
                if gridM[i][k] != gridM[i][k + 1] or gridM[i][k] == 0:
                    ok = False
            if ok == True:
                if gridM[i][j] == 1:
                    return 1
                return 2
    for i in range(7):
        for j in range(3):
            ok = True
            for k in range(j, j + 3):
                if gridM[k][i] != gridM[k + 1][i] or gridM[k][i] == 0:
                    ok = False
            if ok == True:
                if gridM[j][i] == 1:
                    return 1
                return 2
    for i in range(3):
        for j in range(4):
            ok = True
            for k in range(3):
                if gridM[i + k][j + k] != gridM[i + k + 1][j + k + 1] or gridM[i + k][j + k] == 0:
                    ok = False
            if ok == True:
                if gridM[i][j] == 1:
                    return 1
                return 2
    for i in range(3, 6):
        for j in range(4):
            ok = True
            for k in range(3):
                if gridM[i - k][j + k] != gridM[i - k - 1][j + k + 1] or gridM[i - k][j + k] == 0:
                    ok = False
            if ok == True:
                if gridM[i][j] == 1:
                    return 1
                return 2
    return 0

def cls(): print ("\n") * 100


class GraphicDesign():
    def __init__(self, game=None, size=None, name=None):
        if size != None:
            self.size = size
        self.name = name
        self.wind = tk.Tk()
        self.height = 500
        self.width = 500
        self.posX = 0
        self.data = None
        self.posY = 0
        self.file = -1
        self.images = {}
        self.socket = None
        self.board_five = []
        self.finale = False
        for index in range(6):
            arb = []
            for index_j in range(7):
                arb.append(0)
            self.board_five.append(arb)
    def prepare_login_screen(self):
        self.lbl = tk.Label(self.wind, text="First Name").grid(row=0)
    def login_message(self, name, password):
        message = str(messages["login"]) + "&" + name + "&" + password + "&"
        return message.encode('ascii')
    def create_final_task(self, number):
        if not self.finale:
            self.wind.destroy()
            self.wind = tk.Tk()
            self.wind.geometry("500x500")
            if self.my_turn + 1 == number:
                msg = "Congrats you win!!"
            else:
                msg = "Fail you loose!!"
            fd = final_frame.FinalFrame(self.wind, msg)
            self.finale = True
    def create_login_window(self):
        self.wind.geometry("500x500")
        fr = login_frame.LoginFrame(self.wind, self.socket, self.login_message, messages)
        while not fr.logged:
            self.wind.update_idletasks()
            self.wind.update()
        self.wind.destroy()
        self.wind = tk.Tk()
        self.wind.geometry("500x500")
        fd = home_frame.HomeFrame(self.wind, self.socket, messages, fr.name, fr.score)
        while not fd.enetered_queue:
            self.wind.update_idletasks()
            self.wind.update()
        print("Entered queue")
        new_socket = self.is_socket_full(self.socket)
        t0 = time.time()

        while new_socket == -1:
            fd.queueu_message.set("Queue time")
            x = datetime.datetime.now()
            new_socket = self.is_socket_full(self.socket)
            t1 = time.time()
            fd.timer.set(int(t1 - t0))
            self.wind.update_idletasks()
            self.wind.update()
            time.sleep(0.1)
        self.data = new_socket
        self.wind.destroy()
        self.wind = tk.Tk()

    def date_format(self, current_date):
        arb = "00:00"
        index = 1
        int_timer = int(current_date)
        while int_timer >= 0:
            arb[len(arb) - index] = chr(int_timer % 10)
            int_timer /= 10
        return arb
    def login_screen(self):
        data = None
        #while data != str(messages["request_approved"]):
        self.socket.send(self.login_message("ionica", "123"))
        data = str(self.socket.recv(1024).decode("utf-8"))
        if data == str(messages["invalid_credentials"]):
            print("Invalid credentials")
        else:
            print("Connecting")
    def is_socket_full(self, socket):
        message_recieved = None
        is_readable = [socket]
        is_writable = []
        is_error = []
        is_socket_full, w, e = select.select(is_readable, is_writable, is_error, 0.01)
        if is_socket_full:
            message_recieved = socket.recv(1024)
            print(message_recieved)
            return message_recieved
        return -1
    def prepare_game(self):
        print("Waiting!")
        host = '127.0.0.1'
        port = 12345
        #self.wind.update_idletasks()
       # self.wind.update()
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((host,port))
        self.create_login_window()
       # self.prepare_login_screen()
        self.name = get_random_code()
        #self.login_screen()
       # while self.data == None:
           # self.lastInstruction()
        #    self.data = self.socket.recv(1024)
        print("This player plays with", int(self.data))
        self.my_turn = 0
        self.current_turn = 0
        if int(self.data) == 2:
            self.my_turn = 1
        self.data = None
    def clasifyPieces(self, pieces):
        for piece in pieces:
            self.images[piece] = Image.open(piece.imagePath).resize((self.deltaY - 1, self.deltaX - 1))
    def receiveTable(self):
        for indi in range(len(self.table)):
            for indj in range(len(self.table[indi])):
                if self.table[indi][indj] in self.images:
                    self.drawAt(indi, indj, self.images[self.table[indi][indj]])
    def downloadFile(self, file, index):
        img = Image.open(file).resize((self.deltaY - 1, self.deltaX - 1))
        self.image = ImageTk.PhotoImage(img)
        self.images[index] = self.image
        print(self.image)
    def createCanvasGrid(self):
        self.canvas = tk.Canvas(self.wind, height = self.height, width = self.width, bg='white')
        self.canvas.place(relx=0.03, rely=0.03)
    def setSize(self):
        if self.size != None:
            self.wind.geometry(str(self.size[0]) + "x" + str(self.size[1]))
    def createWindow(self):
        self.wind.title(self.name)
        self.setSize()
    def check_if_data_has_been_reacheved(self):
        message_recieved = None
        is_readable = [self.socket]
        is_writable = []
        is_error = []
        is_socket_full, w, e = select.select(is_readable, is_writable, is_error, 0.01)
        if is_socket_full:
            message_recieved = self.socket.recv(1024)
        if message_recieved != None:
            self.message_process(message_recieved)
            self.wind.update()
    def lastInstruction(self):
        while True:
            print(check(self.board_five))
            if check(self.board_five):
                self.create_final_task(check(self.board_five))
            if not self.finale:
                self.show_map()
                self.check_if_data_has_been_reacheved()
            self.wind.update_idletasks()
            self.wind.update()
            time.sleep(0.01)
    def createLine(self, x, y, xF, yF):
        self.canvas.create_line([(x, y), (xF, yF)], tag="grid_line")
    def getDivider(self, sizer, number):
        a = sizer
        b = sizer
        while a % number != 0 and b % number != 0:
            a += 1
            b -= 1
        if a % number == 0:
            return a
        if b % number == 0:
            return b
    def motion(self, event):
        self.posX, self.posY = event.x, event.y
        self.getPosRelativeToCanvasO()
    def image_to_show(self, index):
        if index == players_indexes["player_one"]:
            return self.images[4]
        if index == players_indexes["player_two"]:
            return self.images[5]
        return None

    def show_map(self):
       # print(check(self.board_five))
        for i in range(6):
            for j in range(7):
                print(self.board_five[i][j], end=" ")
            print()
    def message_process(self, message):
        message = str(message.decode("utf-8"))
        processed_message = int(message[0])
        if processed_message == messages["request_approved"]:
            player_index = int(message[1])
            posY = int(message[2])
            posX = int(message[3])
            self.board_five[posY][posX] = player_index
            self.current_turn = not self.current_turn
            self.drawAt(posY, posX, self.image_to_show(player_index))
            return True
        if processed_message == messages["inform_player"]:
            player_index = int(message[1])
            posY = int(message[2])
            posX = int(message[3])
            self.board_five[posY][posX] = player_index
            self.current_turn = not self.current_turn
            self.drawAt(posY, posX, self.image_to_show(player_index))
            return True
        if processed_message == messages["position_not_available"]:
            print("This position is not available")
            return False
        if processed_message == messages["invalid_request"]:
            print("This request is not valid")
            return False
    def compose_request_message(self, posY, posX, player):
        return str(messages["request_position"]) + str(player) + str(posX)
    #JUST A FRONT-END TEST FOR EASY GAMEPLAY 0
    def getPosRelativeToCanvasO(self):
        if self.my_turn == self.current_turn:
            self.posX = self.posX // self.deltaY
            self.posY = self.posY // self.deltaX
            message_to_send = self.compose_request_message(self.posY, self.posX, self.my_turn + 1)
            print(message_to_send)
            self.socket.send(message_to_send.encode('ascii'))
            message_recieved = self.socket.recv(1024)
            print(message_recieved)
            self.message_process(message_recieved)
            self.show_map()
            self.wind.update()
    #JUST A FRONT-END TEST FOR EASY GAMEPLAY
    def bind(self):
        self.canvas.bind('<Button-1>', self.motion)
    def createGrid(self, x, y):
        self.x = x
        self.y = y
        self.height = self.getDivider(self.height, y)
        self.width = self.getDivider(self.width, x)
        self.createCanvasGrid()
        self.deltaX = self.width // x
        self.deltaY = self.height // y
        for index in range(0, self.height, self.deltaY):
            self.createLine(index, 0, index, index + self.width)
        for index in range(0, self.width, self.deltaX):
            self.createLine(0, index, index + self.height, index)
    def autoTraining(self):
        self.game.trainBotTwo(matches=10, batches=10)
    def loadEnemyBot(self):
        self.game.botTwoLoadFile()
        print(len(self.game.bot_player_two.state_dict))
    def saveEnemyBot(self):
        self.game.botTwoSaveFile()
        print(len(self.game.bot_player_two.state_dict))
    def drawAt(self, x, y, img):
        self.canvas.create_image(y * self.deltaY + 1, x * self.deltaX + 1, anchor=NW, image=img)

window = GraphicDesign(size = (535, 535), name = "BoardTool")
window.prepare_game()
window.createWindow()
window.createGrid(6, 7)
window.downloadFile("xmodel.png", 4)
window.downloadFile("omodel.png", 5)
window.bind()
window.lastInstruction()