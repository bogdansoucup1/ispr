import sqlite3
from sqlite3 import Error
import Utils
import random

class Model(object):
    def __init__(self):
        self.conn = None
        self.db_file = r"C:\Users\Bogdan\Desktop\ISPR\users.db"
    def create_connection(self):
        try:
            self.conn = sqlite3.connect(self.db_file)
          #  print(sqlite3.version)
        except Error as e:
            print(e)
        finally:
            return 0

    def create_table(self, create_table_sql):
        try:
            c = self.conn.cursor()
            c.execute(create_table_sql)
            self.conn.commit()
        except Error as e:
            print(e)

    def insert_record(self, name, password, score):
        try:
            id_init = """SELECT MAX(id) FROM Users"""
            a = self.conn.cursor()
            a.execute(id_init)
            response = a.fetchone()
            sqlite_insert_query = """INSERT INTO `Users` ('id', 'name', 'password', 'score')  VALUES  (""" + str(Utils.get_max_id_from_str(response) + 1) + """,'""" + name + """', '""" + password + """', """ + str(score) + """)"""
            c = self.conn.cursor()
            c.execute(sqlite_insert_query)
            self.conn.commit()
        except Error as e:
            print(e)

    def find_by(self, record_name, value):

        try:
            if isinstance(value, str):
                sql_retrieve_query = """SELECT * from Users where """ + record_name + """ = '""" + str(value) + """'"""
            else:
                sql_retrieve_query = """SELECT * from Users where """ + record_name + """ = """ + str(value) + """"""
            c = self.conn.cursor()
            c.execute(sql_retrieve_query)
            response = c.fetchone()
            return response
        except Error as e:
            print(e)

    def find(self, id):
        try:
            sql_retrieve_query = """SELECT * from Users where id = """ + str(id) + """"""
            c = self.conn.cursor()
            c.execute(sql_retrieve_query)
            response = c.fetchone()
            return response
        except Error as e:
            print(e)

    def generate_token(self, response):
        token = ""
        index = 64
        for current_index in range(128):
            if index == current_index:
                token += chr(response[0])
            else:
                token += chr(random.randint(97, 122))
        return token

    def get_user_from_token(self, token):
        try:
            user_id = int.from_bytes(bytes(token[64], encoding='utf8'), "big")
            sql_retrieve_query = """SELECT * from Users where id = """ + str(user_id) + """"""
            c = self.conn.cursor()
            c.execute(sql_retrieve_query)
            response = c.fetchone()
            return response
        except Error as e:
            print(e)

    def login(self, name, password):
        try:
            sql_retrieve_query = """SELECT * from Users where name = '""" + name + """' AND password = '""" + password + """'"""
            c = self.conn.cursor()
            c.execute(sql_retrieve_query)
            response = c.fetchone()
            if response == None:
                return "Wrong password or username"
            return self.generate_token(response)
        except Error as e:
            print(e)

    #if __name__ == '__main__':
    #    create_connection(r"C:\Users\Bogdan\Desktop\ISPR\users.db")
        # table_query = """ CREATE TABLE IF NOT EXISTS Users (
        #                                     id integer PRIMARY KEY,
        #                                     name text NOT NULL,
        #                                     password text,
        #                                     score integer
        #                                 ); """
#arb = Model()
#arb.create_connection()
#print(arb.login("ionica", "123"))
   # create_table(table_query)table_query
#arb.insert_record("a", "1", 1200)
    #print(find_by("id", 4))
    #insert_record("ionica", "123", 43)
    #token = login("ionica3", "123")
   # print(token)
    #print(get_user_from_token(token))