import tkinter as tk
from tkinter import Tk, Canvas, Frame, BOTH, NW
from PIL import Image, ImageTk
import RestrictionFile as rf
import Game as gm

import socket
TCP_IP = '127.0.0.1'
TCP_PORT = 5555
BUFFER_SIZE = 1024
MESSAGE = "Hello from python".encode('UTF-8')

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))

class GraphicDesign():
    def __init__(self, game=None, size=None, name=None):
        if size != None:
            self.size = size
        self.name = name
        self.wind = tk.Tk()
        self.height = 500
        self.width = 500
        self.posX = 0
        self.posY = 0
        self.file = -1
        self.images = {}
        self.game = game
    def clasifyPieces(self, pieces):
        for piece in pieces:
            self.images[piece] = Image.open(piece.imagePath).resize((self.deltaY - 1, self.deltaX - 1))
    def receiveTable(self):
        for indi in range(len(self.table)):
            for indj in range(len(self.table[indi])):
                if self.table[indi][indj] in self.images:
                    self.drawAt(indi, indj, self.images[self.table[indi][indj]])
    def downloadFile(self, file, index):
        img = Image.open(file).resize((self.deltaY - 1, self.deltaX - 1))
        self.image = ImageTk.PhotoImage(img)
        self.images[index] = self.image
        print(self.image)
    def createCanvasGrid(self):
        self.canvas = tk.Canvas(self.wind, height = self.height, width = self.width, bg='white')
        self.canvas.place(relx=0.03, rely=0.03)
    def setSize(self):
        if self.size != None:
            self.wind.geometry(str(self.size[0]) + "x" + str(self.size[1]))
    def createWindow(self):
        self.wind.title(self.name)
        self.setSize()
    def lastInstruction(self):
        self.wind.mainloop()
    def createLine(self, x, y, xF, yF):
        self.canvas.create_line([(x, y), (xF, yF)], tag="grid_line")
    def getDivider(self, sizer, number):
        a = sizer
        b = sizer
        while a % number != 0 and b % number != 0:
            a += 1
            b -= 1
        if a % number == 0:
            return a
        if b % number == 0:
            return b
    def motion(self, event):
        self.posX, self.posY = event.x, event.y
        #MESSAGE = (str(self.posX) + str(self.posY)).encode('UTF-8')
        #s.send(MESSAGE)
       # data = s.recv(BUFFER_SIZE)

        self.getPosRelativeToCanvasO()
        #self.getPosRelativeToCanvasX()
    #JUST A FRONT-END TEST FOR EASY GAMEPLAY 0
    def getPosRelativeToCanvasO(self):
        self.posX = self.posX // self.deltaY
        self.posY = self.posY // self.deltaX
        #print(self.posY, self.posX)

        MESSAGE = (str(self.posX) + str(self.posY)).encode('UTF-8')
        s.send(MESSAGE)
        print(self.posY, self.posX)

        self.drawAt(self.posY, self.posX, self.images[4])

        #print(data[1], data[0])
        self.game.objec.board_to_arr = self.game.player_one.execute_action_form_query_array(self.game.objec.board_to_arr, (self.posY * self.x + self.posX, 4, "Spawned"))
        self.game.objec.convert_arr_to_table()
        #self.receiveTable()
        data = s.recv(BUFFER_SIZE)
        #self.game.bot_player_two.saveGameState(self.game.objec.board_to_arr)
        #print(rf.board_five(self.game.objec.board_to_arr, self.game.player_one))
        #self.make_bot_two_move()
        self.game.objec.board_to_arr = self.game.player_two.execute_action_form_query_array(self.game.objec.board_to_arr, (data[1] * self.x + data[0], 5, "Spawned"))
        self.drawAt(data[1], data[0], self.images[5])

        self.game.objec.convert_arr_to_table()
        print(rf.board_five(self.game.objec.board_to_arr, self.game.player_one))
        #self.receiveTable()
    #JUST A FRONT-END TEST FOR EASY GAMEPLAY0
    def make_bot_one_move(self):
        if rf.board_five(self.game.objec.board_to_arr, self.game.player_one) == 0 and rf.drawFor(self.game.objec.board_to_arr, self.game.player_one) == 0:
            self.game.makebotPlayerOneAction()
        self.game.objec.convert_arr_to_table()
        self.game.bot_player_one.saveGameState(self.game.objec.board_to_arr)
    def make_bot_two_move(self):
        if rf.board_five(self.game.objec.board_to_arr, self.game.player_two) == 0 and rf.drawFor(self.game.objec.board_to_arr, self.game.player_two) == 0:
            self.game.makebotPlayerTwoAction()
        #print(self.game.bot_player_two.getList())
        self.game.objec.convert_arr_to_table()
        self.game.bot_player_two.saveGameState(self.game.objec.board_to_arr)
    #JUST A FRONT-END TEST FOR EASY GAMEPLAY
    def getPosRelativeToCanvasX(self):
        #print(rf.board_five(self.game.objec.board_to_arr, self.game.player_one))
        self.posX = self.posX // self.deltaY
        self.posY = self.posY // self.deltaX
        self.game.objec.board_to_arr = self.game.player_two.execute_action_form_query_array(self.game.objec.board_to_arr, (self.posY * self.y + self.posX, 5, "Spawned"))
        self.game.bot_player_one.saveGameState(self.game.objec.board_to_arr)
        self.make_bot_one_move()
        print(rf.board_five(self.game.objec.board_to_arr, self.game.player_two))
        self.receiveTable()

    #JUST A FRONT-END TEST FOR EASY GAMEPLAY
    def bind(self):
        self.canvas.bind('<Button-1>', self.motion)
    def createGrid(self, x, y):
        self.x = x
        self.y = y
        self.height = self.getDivider(self.height, y)
        self.width = self.getDivider(self.width, x)
        self.createCanvasGrid()
        self.deltaX = self.width // x
        self.deltaY = self.height // y
        for index in range(0, self.height, self.deltaY):
            self.createLine(index, 0, index, index + self.width)
        for index in range(0, self.width, self.deltaX):
            self.createLine(0, index, index + self.height, index)
        self.game = gm.Game(size=(x, y), name1="Ana", name2="Dorel")
        self.game.addPieceTooPlayerOne("X", "Spawned", rf.formPiece)
        self.game.addPieceTooPlayerTwo("O", "Spawned", rf.formPiece)
        self.game.botInit(2, 4, rf.board_five, rf.drawFor)
        #self.game.addRestrictionBotTwo(rf.restriction_one)
        self.game.prepareTrainingForBotTwo(rf.restriction_one, rf.restriction_one)
        self.game.prepareTrainingForBotOne(rf.restriction_one, rf.restriction_one)
        self.game.setResetBotTwo(600)
        self.game.setBoundariesForBotTwo(10, -10)
        self.table = self.game.objec.board
    def autoTraining(self):
        self.game.trainBotTwo(matches=10, batches=10)
    def loadEnemyBot(self):
        self.game.botTwoLoadFile()
        print(len(self.game.bot_player_two.state_dict))
    def saveEnemyBot(self):
        self.game.botTwoSaveFile()
        print(len(self.game.bot_player_two.state_dict))
    def drawAt(self, x, y, img):
        self.canvas.create_image(y * self.deltaY + 1, x * self.deltaX + 1, anchor=NW, image=img)
print("start")
#rf.loadDLL()
window = GraphicDesign(size = (1040, 880), name = "BoardTool")
window.createWindow()
window.createGrid(6, 7)
window.downloadFile("first.png", 4)
window.downloadFile("second.png", 5)
#window.loadEnemyBot()
#print(window.game.bot_player_two.state_dict)
#exit(0)
#window.autoTraining()
window.bind()
#window.make_bot_one_move()
window.receiveTable()
window.lastInstruction()
#window.game.bot_player_one.learnFromMistakesInclination()
#window.saveEnemyBot()
print("stop")