from tkinter import *
import tkinter.messagebox as tm
import datetime


class HomeFrame(Frame):
    def __init__(self, master, socket, messages, name, score):
        super().__init__(master)

       # self.label_username = Label(self, text="Username")
        self.name = name
        self.score = score
        self.label_message = Label(master, text="Hello " + self.name + " your current score is " + str(self.score))
        self.timer = StringVar()
        self.queueu_message = StringVar()
        self.label_time = Label(master, text="", textvariable=self.timer)

        self.timer_message = Label(master, text="", textvariable=self.queueu_message)

        self.label_message.place(relx=0.5, rely=0.3, anchor='n')
        self.label_time.place(relx=0.5, rely=0.4, anchor='n')
        self.timer_message.place(relx=0.4, rely=0.4, anchor='n')

        self.logbtn = Button(master, text="Find match", command=self._login_btn_clicked)
        self.logbtn.place(relx=0.5, rely=0.35, anchor='n')
        self.socket = socket
        self.messages = messages
        self.enetered_queue = False
        self.entered_match = False
        self.pack()

    def _login_btn_clicked(self):
        # print("Clicked")
        print("Waiting to find a partener!!!")
        if not self.enetered_queue:
            self.socket.send(str(self.messages["add_to_queue"]).encode("ascii"))
        self.enetered_queue = True


# root = Tk()
# lf = LoginFrame(root)
# root.mainloop()